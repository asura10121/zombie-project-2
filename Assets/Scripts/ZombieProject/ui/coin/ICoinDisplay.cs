﻿namespace ZombieProject.ui.coin
{
    public interface ICoinDisplay
    {
        void SetCoin(int previous, int current);
    }
}