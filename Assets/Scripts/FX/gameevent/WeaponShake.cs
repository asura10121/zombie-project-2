﻿using UnityEngine;
using ZombieProject.events;
using ZombieProject.weapon;

namespace FX.gameevent
{
    public class WeaponShake : MonoBehaviour
    {
        [SerializeField] private float amp = 0.5f;
        [SerializeField] private float shakeLength = 0.2f;

        private void OnEnable()
        {
            GameEventInstance.Instance.WeaponEvent.ShootEvent += OnShootEvent;
        }

        private void OnDisable()
        {
            GameEventInstance.Instance.WeaponEvent.ShootEvent -= OnShootEvent;
        }

        private void OnShootEvent(IWeapon weapon)
        {
            ShakeCam.Instance.Shake(amp, shakeLength);
        }
    }
}