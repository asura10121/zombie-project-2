﻿using UnityEngine;
using ZombieProject.tower.builder;

namespace ZombieProject.tower
{
    public class StrategyPoint : MonoBehaviour
    {
        [SerializeField] private TowerSo towerSo;

        private void Start()
        {
            GetComponent<ITowerBuilder>().Build(towerSo);
        }
    }
}