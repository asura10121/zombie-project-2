﻿using Core.entity;
using Core.events;
using UnityEngine;

namespace FX.gameevent
{
    public class MobHitFX : MonoBehaviour
    {
        private void OnEnable()
        {
            EventInstance.Instance.EntityEvent.EntityDamageModifiedEvent += OnEntityDamageModifiedEvent;
            EventInstance.Instance.EntityEvent.EntityDamageEvent += OnEntityDamageEvent;
            EventInstance.Instance.EntityEvent.EntityDeathEvent += OnEntityDeathEvent;
        }


        private void OnDisable()
        {
            EventInstance.Instance.EntityEvent.EntityDamageModifiedEvent -= OnEntityDamageModifiedEvent;
            EventInstance.Instance.EntityEvent.EntityDamageEvent -= OnEntityDamageEvent;
            EventInstance.Instance.EntityEvent.EntityDeathEvent -= OnEntityDeathEvent;
        }

        private void OnEntityDamageEvent(LivingEntity livingEntity, float damage)
        {
            FXInstance.PlayEffect(ParticleType.BLOOD, livingEntity.GetLocation());
        }

        private void OnEntityDamageModifiedEvent(LivingEntity livingEntity, float damage, bool modified)
        {
            FXInstance.PlayEffect(ParticleType.BLOOD, livingEntity.GetLocation());
        }

        private void OnEntityDeathEvent(LivingEntity livingEntity)
        {
            FXInstance.PlayEffect(ParticleType.DEAD, livingEntity.GetLocation());
        }
    }
}