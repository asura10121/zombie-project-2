﻿using Core.ui.mainmenu.main;
using UnityEngine;

namespace Core.ui.mainmenu
{
    public class MainMenuManager : MonoBehaviour
    {
        [SerializeField] private MainMenu mainMenu;

        private void Start()
        {
            mainMenu.Show();
        }
    }
}