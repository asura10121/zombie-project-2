﻿using UnityEngine;

namespace Utils
{
    public static class GamePaused
    {
        public static void Paused()
        {
            Time.timeScale = 0f;
        }

        public static void Continue()
        {
            Time.timeScale = 1f;
        }
    }
}