﻿using Singleton;
using UnityEngine;

namespace Player.visual
{
    public class PlayerHand : MonoSingleton<PlayerHand>
    {
        [SerializeField] private Transform handTransform;

        private Quaternion _ori;
        public Transform HandTransform => handTransform;

        public override void Awake()
        {
            base.Awake();

            _ori = handTransform.localRotation;
            Debug.Assert(handTransform != null, "handTransform can't be null!");
        }

        public void SwingHand(float power, float startLength, float endLenght)
        {
            LeanTween.cancel(gameObject);
            handTransform.localRotation = _ori;
            var gunPower = -(handTransform.localRotation.eulerAngles + new Vector3(power, 0));
            LeanTween.rotateLocal(gameObject, gunPower, startLength)
                .setOnComplete(() => { LeanTween.rotateLocal(gameObject, _ori.eulerAngles, endLenght); });
        }
    }
}