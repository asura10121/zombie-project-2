﻿using UnityEngine;
using ZombieProject.tower;

namespace Core.entity.livingentity
{
    public class SoldierMeleeAttackResponse : MonoBehaviour, ILivingEntityAttackResponse
    {
        private float _timer;
        private BarrackSo _barrackSo;

        public void Init(BarrackSo barrackSo)
        {
            _barrackSo = barrackSo;
        }

        public void Attack(LivingEntity attacker, Entity target, float damage)
        {
            if (!(target is LivingEntity livingEntity)) return;
            //Look At Target
            var dir = target.GetLocation() - attacker.GetLocation();
            dir.y = 0f;
            dir.Normalize();
            var rotation = Quaternion.LookRotation(dir);
            attacker.transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime);

            _timer -= Time.deltaTime;
            if (_timer > 0) return;
            _timer = _barrackSo.attackSpeed;
            damage = Random.Range(_barrackSo.minDamage, _barrackSo.maxDamage);

            attacker.SwingHands();
            livingEntity.Damage(damage, attacker, false);
        }
    }
}