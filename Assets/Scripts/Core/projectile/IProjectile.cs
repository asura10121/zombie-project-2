﻿using Core.entity;
using UnityEngine;

namespace Core.projectile
{
    public interface IProjectile
    {
        void Init(Entity shooter, Transform shootTransform,float damage, Vector3 velocity, ProjectileType projectileType);
        Entity GetShooter();
        float GetDamage();
        ProjectileType GetProjectileType();
    }
}