﻿
using System;
using Core.events;
using Core.ui.pausedmenu;
using UnityEngine;
using ZombieProject.events;

namespace Player.controller
{
    public class PlayerGUIController : MonoBehaviour
    {
        private Core.entity.Player _player;
        private PausedMenu _pausedMenu;

        private bool _isShowMenu;

        private void Start()
        {
            _player = FindObjectOfType<Core.entity.Player>();
            _pausedMenu = FindObjectOfType<PausedMenu>();
        }

        private void OnEnable()
        {
            EventInstance.Instance.InputEvent.InventoryEvent += OnInventoryEvent;
            EventInstance.Instance.InputEvent.ToggleMenuEvent += OnToggleMenuEvent;
            EventInstance.Instance.UIEvent.ToggleMenuEvent += OnToggleMenuEvent;
            EventInstance.Instance.UIEvent.ToggleMenuEvent += OnToggleMenuEvent;
            GameEventInstance.Instance.GameEvent.GameOverEvent += OnGameOverEvent;
        }

        private void OnDisable()
        {
            EventInstance.Instance.InputEvent.InventoryEvent -= OnInventoryEvent;
            EventInstance.Instance.InputEvent.ToggleMenuEvent -= OnToggleMenuEvent;
            EventInstance.Instance.UIEvent.ToggleMenuEvent -= OnToggleMenuEvent;
            GameEventInstance.Instance.GameEvent.GameOverEvent -= OnGameOverEvent;
        }

        private bool _isGameOver;

        private void OnGameOverEvent(bool win)
        {
            _isGameOver = true;
        }

        private void OnInventoryEvent()
        {
            if (_isShowMenu || _isGameOver) return;
            var openInventory = _player.GetOpenInventory();
            if (openInventory != null)
            {
                _player.CloseInventory();
                return;
            }

            _player.OpenInventory(_player.GetInventory());
        }

        private void OnToggleMenuEvent()
        {
            if (_isGameOver) return;
            var openInventory = _player.GetOpenInventory();
            if (openInventory != null)
            {
                _player.CloseInventory();
                return;
            }

            _isShowMenu = !_isShowMenu;
            if (_isShowMenu) _pausedMenu.Show();
            else _pausedMenu.Hide();
        }

        private void OnToggleMenuEvent(bool show)
        {
            _isShowMenu = show;
        }
    }
}