﻿using Core.events;
using Core.inventory.InventoryView;
using UnityEngine;
using Utils;
using ZombieProject.events;

namespace Player.controller
{
    public class GameCursorController : MonoBehaviour
    {
        private void OnEnable()
        {
            EventInstance.Instance.InventoryEvent.InventoryOpenEvent += OnInventoryOpenEvent;
            EventInstance.Instance.InventoryEvent.InventoryCloseEvent += OnInventoryCloseEvent;
            GameEventInstance.Instance.GameEvent.GameOverEvent += OnGameOverEvent;
            EventInstance.Instance.UIEvent.ToggleMenuEvent += OnToggleMenuEvent;
        }

        private void OnDisable()
        {
            EventInstance.Instance.InventoryEvent.InventoryOpenEvent -= OnInventoryOpenEvent;
            EventInstance.Instance.InventoryEvent.InventoryCloseEvent -= OnInventoryCloseEvent;
            GameEventInstance.Instance.GameEvent.GameOverEvent -= OnGameOverEvent;
            EventInstance.Instance.UIEvent.ToggleMenuEvent -= OnToggleMenuEvent;
        }

        private void Start()
        {
            GameCursor.Hide();
        }

        private void OnInventoryOpenEvent(IInventoryView inventoryView)
        {
            GameCursor.Show();
        }

        private void OnInventoryCloseEvent()
        {
            GameCursor.Hide();
        }

        private void OnGameOverEvent(bool win)
        {
            GameCursor.Show();
        }

        private void OnToggleMenuEvent(bool show)
        {
            if (show) GameCursor.Show();
            else GameCursor.Hide();
        }
    }
}