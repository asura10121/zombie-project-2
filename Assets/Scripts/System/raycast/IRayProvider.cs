﻿using UnityEngine;

namespace System.raycast
{
    public interface IRayProvider
    {
        Ray GetRay();
    }
}