﻿using Cinemachine;
using Core.entity;
using Core.events;
using Core.spectator;
using Player;
using UnityEngine;
using ZombieProject.events;
using ZombieProject.game.wave;

namespace ZombieProject.game
{
    public class PlayerRespawn : MonoBehaviour, ISpectator
    {
        [SerializeField] private CinemachineVirtualCamera virtualCamera;
        [SerializeField] private int punishHealthPoint = 1;
        [SerializeField] private float respawnDelay;
        private IWaveManager _waveManager;
        private Core.entity.Player _player;
        private Vector3 _spawnPoint;

        private void Awake()
        {
            _waveManager = FindObjectOfType<WaveManager>();
            _player = FindObjectOfType<Core.entity.Player>();
        }

        private void Start()
        {
            _spawnPoint = _player.GetLocation();
        }

        private void OnEnable()
        {
            EventInstance.Instance.PlayerEvent.PlayerHitMonsterEvent += OnPlayerHitMonsterEvent;
            GameEventInstance.Instance.GameEvent.GameOverEvent += OnGameOverEvent;
        }


        private void OnDisable()
        {
            EventInstance.Instance.PlayerEvent.PlayerHitMonsterEvent -= OnPlayerHitMonsterEvent;
            GameEventInstance.Instance.GameEvent.GameOverEvent -= OnGameOverEvent;
        }

        private void OnPlayerHitMonsterEvent(Monster monster)
        {
            Respawn();
        }

        private LTDescr _respawnLean;

        private void Respawn()
        {
            _player.SetLocation(_spawnPoint);
            _waveManager.HealthPoint -= punishHealthPoint;
            PlayerSpectator.Instance.SetCamera(this);
            _respawnLean = LeanTween.value(respawnDelay, 0, respawnDelay)
                .setOnUpdate(v => { GameEventInstance.Instance.GameEvent.OnPlayerRespawnTimerEvent(v, respawnDelay); })
                .setOnComplete(() =>
                {
                    PlayerSpectator.Instance.SetCamera(_player);
                    GameEventInstance.Instance.GameEvent.OnPlayerRespawnEvent();
                });
        }

        private void OnGameOverEvent(bool win)
        {
            if (_respawnLean != null)
                LeanTween.cancel(gameObject);
        }

        public CinemachineVirtualCamera GetCamera()
        {
            return virtualCamera;
        }
    }
}