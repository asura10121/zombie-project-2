﻿using System;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace ZombieProject.ui.damageindicator
{
    public class DamageIndicator : MonoBehaviour
    {
        [SerializeField] private float power;
        [SerializeField] private float range;
        [SerializeField] private TextMeshProUGUI damageText;
        public Action<DamageIndicator> OnHitFloor;

        private int _boundLeft;
        private Rigidbody _rigidbody;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            OnHitFloor = indicator => { };
        }

        private void ApplyVelocity()
        {
            _rigidbody.angularVelocity = Vector3.zero;
            _rigidbody.velocity = Vector3.zero;
            _rigidbody.AddForce(
                new Vector3(Random.Range(range * 0.5f, range), power, Random.Range(range * 0.5f, range)),
                ForceMode.Impulse);
        }

        public void Init(float damage, Color color)
        {
            _boundLeft = 1;
            damageText.color = color;
            damageText.text = $"{Mathf.RoundToInt(damage)}";

            ApplyVelocity();
        }

        private void OnCollisionStay(Collision other)
        {
            if (_boundLeft-- > 0) return;
            OnHitFloor?.Invoke(this);
        }
    }
}