﻿namespace Core.spectator
{
    public static class Spectator
    {
        public static void SetSpectate(ISpectator from, ISpectator to)
        {
            from.GetCamera().Priority = -1;
            to.GetCamera().Priority = 100;
        }

        public static void SetSpectate(ISpectator to)
        {
            to.GetCamera().Priority = 100;
        }
    }
}