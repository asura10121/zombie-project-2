﻿using Core.events;

namespace ZombieProject.weapon
{
    public interface IWeaponInteract
    {
        void OnInteract(IWeapon weapon, ActionType actionType);
    }
}