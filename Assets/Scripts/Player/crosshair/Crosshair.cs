﻿using System;
using System.Collections.Generic;
using Singleton;
using UnityEngine;
using UnityEngine.UI;

namespace Player.crosshair
{
    public class Crosshair : MonoSingleton<Crosshair>
    {
        [SerializeField] private CrosshairData[] crosshairData;

        private Dictionary<CrosshairType, Image> _crosshairs;

        public override void Awake()
        {
            base.Awake();
            InitCrosshair();
        }

        private void Start()
        {
            ResetDefault();
        }

        private void InitCrosshair()
        {
            _crosshairs = new Dictionary<CrosshairType, Image>();

            foreach (var data in crosshairData)
            {
                if (_crosshairs.ContainsKey(data.crosshariType)) continue;
                _crosshairs.Add(data.crosshariType, data.image);
                data.image.gameObject.SetActive(false);
            }
        }

        public void ResetDefault()
        {
            SetCrosshair(CrosshairType.DEFAULT);
        }

        public void SetCrosshair(CrosshairType crosshairType)
        {
            foreach (var image in _crosshairs.Values)
            {
                image.gameObject.SetActive(false);
            }

            if (_crosshairs.ContainsKey(crosshairType))
            {
                _crosshairs[crosshairType].gameObject.SetActive(true);
            }
        }

        [Serializable]
        public struct CrosshairData
        {
            public CrosshairType crosshariType;
            public Image image;
        }
    }
}