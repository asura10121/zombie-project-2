﻿using Core.inventory;

namespace ZombieProject.tower.inventory.item
{
    public class TowerCommandItem : ItemStack
    {
        private readonly TowerSo _towerSo;

        public TowerCommandItem(EnumValue enumValue, TowerSo towerSo) : base(enumValue)
        {
            _towerSo = towerSo;
        }

        public TowerSo GetTowerSo()
        {
            return _towerSo;
        }
    }
}