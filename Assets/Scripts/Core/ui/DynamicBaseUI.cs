﻿using UnityEngine;

namespace Core.ui
{
    public class DynamicBaseUI : BaseUI
    {
        public override void Awake()
        {
            base.Awake();
            GetComponent<Canvas>().worldCamera = Camera.main;
        }
    }
}