﻿using UnityEngine;

namespace FX
{
    public class CustomParticle : MonoBehaviour
    {
        [SerializeField] private ParticleType particleType;
        public ParticleType ParticleType => particleType;
    }
}