﻿using System.raycast;
using UnityEngine;

namespace Player.raycast
{
    public class PlayerRaycastHitResponse : MonoBehaviour, IRayCastHitResponse, IRaycastHitGetter
    {
        private RaycastHit _hit;

        public void OnRaycastHit(RaycastHit hit)
        {
            _hit = hit;
        }

        public RaycastHit GetRaycastHit()
        {
            return _hit;
        }
    }
}