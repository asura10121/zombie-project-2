﻿using Core.entity;
using UnityEngine;

namespace ZombieProject.weapon
{
    public interface IWeapon
    {
        bool IsReloading { get; set; }
        float ReloadTimeLeft { get; set; }
        bool HasAmmo();
        bool IsFull();
        Entity GetShooter();
        Transform GetShootTransform();
        WeaponSo GetWeaponSo();
        void Shoot();
        void Reload();
        int GetAmmo();
        void SetAmmo(int ammo);
        int GetMaxAmmo();
        void SetMaxAmmo(int maxAmmo);
    }
}