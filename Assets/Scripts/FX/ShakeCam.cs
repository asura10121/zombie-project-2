﻿using Cinemachine;
using Singleton;

namespace FX
{
    public class ShakeCam : MonoSingleton<ShakeCam>
    {
        private CinemachineVirtualCamera _virtualCamera;

        public override void Awake()
        {
            base.Awake();
            _virtualCamera = GetComponent<CinemachineVirtualCamera>();
        }

        private LTDescr _task;

        public void Shake(float intensity, float time)
        {
            if (_task != null)
            {
                LeanTween.cancel(_task.id);
            }

            var multiChannelPerlin = _virtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
            _task = LeanTween.value(1, 0, time)
                .setOnUpdate(v => { multiChannelPerlin.m_AmplitudeGain = intensity * v; })
                .setOnComplete(() => { multiChannelPerlin.m_AmplitudeGain = 0; });
        }
    }
}