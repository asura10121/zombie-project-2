﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using ZombieProject.game.wave;

namespace ZombieProject.ui.nextwaveinfo
{
    public class NextWaveInfo : MonoBehaviour, INextWaveInfo
    {
        [SerializeField] private string titleInfo;
        [SerializeField] private TextMeshProUGUI infoText;

        private void Awake()
        {
            ClearInfo();
        }

        public void SetWaveInfo(WaveSo waveSo)
        {
            var waveInfo = new Dictionary<string, int>();
            foreach (var waveEvent in waveSo.wavesEvents)
            {
                var key = waveEvent.entityType.ToString()
                    .Replace("_", " ");

                if (waveInfo.ContainsKey(key))
                {
                    waveInfo[key] += waveEvent.spawnCount;
                    continue;
                }

                waveInfo.Add(key, waveEvent.spawnCount);
            }

            infoText.text = $"{titleInfo}\n";
            foreach (var data in waveInfo)
            {
                infoText.text += $"{data.Key} x{data.Value}\n";
            }
        }

        public void ClearInfo()
        {
            infoText.text = string.Empty;
        }
    }
}