﻿using Core.events;
using Player;
using UnityEngine;

namespace Core.ui
{
    public abstract class BaseUI : MonoBehaviour, IVisible
    {
        private CanvasGroup _canvasGroup;

        public virtual void Awake()
        {
            _canvasGroup = TryGetComponent<CanvasGroup>(out var canvasGroup)
                ? canvasGroup
                : gameObject.AddComponent<CanvasGroup>();
        }

        protected virtual void Start()
        {
        }

        protected virtual void OnEnable()
        {
            EventInstance.Instance.UIEvent.ToggleUIEvent += OnToggleUIEvent;
        }

        protected virtual void OnDisable()
        {
            EventInstance.Instance.UIEvent.ToggleUIEvent -= OnToggleUIEvent;
        }

        public virtual void Show()
        {
            _canvasGroup.alpha = 1f;
        }

        public virtual void Hide()
        {
            _canvasGroup.alpha = 0f;
        }

        private void OnToggleUIEvent(bool visible)
        {
            if (visible) Show();
            else Hide();
        }
    }
}