﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "New EnumValue", menuName = "Variables/EnumValue", order = 0)]
public class EnumValue : ScriptableObject, IComparable<EnumValue>
{
    public int CompareTo(EnumValue other)
    {
        return other.GetInstanceID().Equals(other.GetInstanceID()) ? 1 : 0;
    }
}