﻿using TMPro;
using UnityEngine;
using ZombieProject.weapon;

namespace ZombieProject.ui.weaponbar
{
    public class WeaponAmmoDisplay : MonoBehaviour, IWeaponAmmoDisplay
    {
        [SerializeField] private TextMeshProUGUI ammoText;
        [SerializeField] private TextMeshProUGUI maxAmmoText;

        [Header("AMMO COLOR")] [SerializeField]
        private int lowAmmoAmount = 5;

        [SerializeField] private Color lowAmmoColor = Color.white;

        private Color _oriColor;

        private void Awake()
        {
            _oriColor = ammoText.color;
            ammoText.text = string.Empty;
        }

        public void SetAmmo(IWeapon weapon)
        {
            var ammo = weapon.GetAmmo();
            ammoText.color = ammo <= lowAmmoAmount ? lowAmmoColor : _oriColor;
            ammoText.text = $"{ammo}";
            maxAmmoText.text = $"{weapon.GetMaxAmmo()}";
        }
    }
}