﻿namespace ZombieProject.game
{
    public enum GameState
    {
        WAITING,
        RUNNING,
        ENDED,
    }
}