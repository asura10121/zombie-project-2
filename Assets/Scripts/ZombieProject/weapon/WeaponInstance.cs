﻿using System;
using System.Collections.Generic;
using Singleton;
using UnityEngine;

namespace ZombieProject.weapon
{
    public class WeaponInstance : ResourceSingleton<WeaponInstance>
    {
        [SerializeField] private WeaponData[] weaponData;
        private Dictionary<WeaponType, Weapon> _weapons;

        public override void Awake()
        {
            base.Awake();
            InitProjectile();
        }

        private void InitProjectile()
        {
            _weapons = new Dictionary<WeaponType, Weapon>();
            foreach (var data in weaponData)
            {
                if (_weapons.ContainsKey(data.weaponType)) continue;
                _weapons.Add(data.weaponType, data.weaponPrefab);
            }
        }

        public static Weapon GetWeapon(WeaponType weaponType)
        {
            if (!Instance._weapons.ContainsKey(weaponType)) return null;
            var weapon = Instantiate(Instance._weapons[weaponType]);
            return weapon;
        }

        [Serializable]
        public struct WeaponData
        {
            public WeaponType weaponType;
            public Weapon weaponPrefab;
        }
    }
}