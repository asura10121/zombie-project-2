﻿using ZombieProject.weapon;

namespace ZombieProject.ui.weaponbar
{
    public interface IWeaponDisplay
    {
        void SetWeapon(WeaponSo weaponSo);
    }
}