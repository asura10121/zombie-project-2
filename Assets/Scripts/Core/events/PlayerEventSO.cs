﻿using System;
using Core.entity;
using Core.inventory;
using Core.spectator;
using UnityEngine;

namespace Core.events
{
    [CreateAssetMenu(menuName = "Event/Player Event Channel")]
    public class PlayerEventSO : ScriptableObject
    {
        public event Action<entity.Player, ActionType, ItemStack> PlayerInteractEvent;
        public event Action<entity.Player, RaycastHit, ActionType, ItemStack> PlayerInteractObjectEvent;
        public event Action<ISpectator> PlayerSpectateEvent;
        public event Action<Monster> PlayerHitMonsterEvent;

        public void OnPlayerInteractObjectEvent(
            entity.Player player, RaycastHit raycastHit, ActionType actionType, ItemStack itemStack)
        {
            PlayerInteractObjectEvent?.Invoke(player, raycastHit, actionType, itemStack);
        }

        public void OnPlayerInteractEvent(entity.Player player, ActionType actionType, ItemStack itemStack)
        {
            PlayerInteractEvent?.Invoke(player, actionType, itemStack);
        }

        public void OnPlayerSpectateEvent(ISpectator spectator)
        {
            PlayerSpectateEvent?.Invoke(spectator);
        }

        public void OnPlayerHitMonsterEvent(Monster monster)
        {
            PlayerHitMonsterEvent?.Invoke(monster);
        }
    }
}