﻿namespace ZombieProject.game.checkpoint
{
    public interface ICheckPoint
    {
        TargetMob GetNextTarget();
    }
}