﻿using Core.ui.gamebutton;
using UnityEngine;
using UnityEngine.UI;
using ZombieProject.weapon;

namespace Core.ui.weaponshop
{
    public class ItemShopSlot : MonoBehaviour
    {
        [Header("ITEM SHOP")] [SerializeField] private GameButton slotGameButton;
        [SerializeField] private Transform purchasedStatusIcon;
        [SerializeField] private Transform equippedStatusIcon;
        [SerializeField] private Image indicator;

        public WeaponSo Weapon { get; set; }
        public IGameButton SlotButton => slotGameButton;

        public void SetPurchased(bool isPurchased)
        {
            purchasedStatusIcon.gameObject.SetActive(isPurchased);
        }

        public void SetEquipped(bool isEquipped)
        {
            equippedStatusIcon.gameObject.SetActive(isEquipped);
        }

        public void SetIndicator(bool show)
        {
            indicator.gameObject.SetActive(show);
        }
    }
}