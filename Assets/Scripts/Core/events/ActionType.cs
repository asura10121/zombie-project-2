﻿namespace Core.events
{
    public enum ActionType
    {
        LEFT_CLICK_DOWN,
        LEFT_CLICK_UP,
        RIGHT_CLICK_DOWN,
        RIGHT_CLICK_UP
    }
}