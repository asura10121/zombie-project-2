﻿using ScriptableObjects.Inventory;
using UnityEngine;
using UnityEngine.UI;
using ZombieProject.weapon;

namespace ZombieProject.ui.weaponbar
{
    public class WeaponDisplay : MonoBehaviour, IWeaponDisplay

    {
        [SerializeField] private ItemIconPack itemIconPack;
        [SerializeField] private Image weaponImage;

        public void SetWeapon(WeaponSo weaponSo)
        {
            weaponImage.sprite = itemIconPack.GetItemIcon(weaponSo.enumValue);
        }
    }
}