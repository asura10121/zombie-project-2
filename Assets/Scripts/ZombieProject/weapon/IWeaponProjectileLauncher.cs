﻿using Core.projectile;
using Core.projectile.rayprojectile;

namespace ZombieProject.weapon
{
    public interface IWeaponProjectileLauncher
    {
        ProjectileBase LaunchProjectile(IWeapon weapon);
    }
}