﻿using System;
using Core.ui.gamebutton;
using Core.ui.mainmenu.settings;
using UnityEngine;
using Utils.scene;

namespace Core.ui.mainmenu.main
{
    public class MainMenu : MonoBehaviour, IVisible
    {
        [SerializeField] private GameButton startButton;
        [SerializeField] private GameButton settingsButton;
        [SerializeField] private SettingsMenu settingsMenu;

        private void Awake()
        {
            Debug.Assert(settingsMenu != null, "ISettingsMenu!=null");

            startButton.SetClickAction(() => { GameSceneManager.LoadScene(GameSceneType.MAP); });
            settingsButton.SetClickAction(settingsMenu.Show);
        }

        private void Start()
        {
            settingsMenu.Hide();
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}