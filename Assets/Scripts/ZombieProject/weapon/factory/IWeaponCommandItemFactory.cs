﻿using ZombieProject.weapon.inventory.item;

namespace ZombieProject.weapon.factory
{
    public interface IWeaponCommandItemFactory : IFactory<WeaponCommandItem, WeaponSo>
    {
    }
}