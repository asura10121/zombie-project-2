﻿using Core.events;
using UnityEngine;

namespace Player
{
    public class PlayerInput : MonoBehaviour
    {
        private void Update()
        {
            //INTERACT
            if (Input.GetMouseButtonDown(0)) EventInstance.Instance.InputEvent.OnAttackEvent(true);

            if (Input.GetMouseButtonUp(0)) EventInstance.Instance.InputEvent.OnAttackEvent(false);

            if (Input.GetMouseButtonDown(1)) EventInstance.Instance.InputEvent.OnUseItemEvent(true);

            if (Input.GetMouseButtonUp(1)) EventInstance.Instance.InputEvent.OnUseItemEvent(false);

            //INVENTORY
            if (Input.GetKeyDown(KeyCode.E)) EventInstance.Instance.InputEvent.OnInventoryEvent();

            if (Input.GetAxis("Mouse ScrollWheel") > 0) EventInstance.Instance.InputEvent.OnPickItemEvent(false);

            if (Input.GetAxis("Mouse ScrollWheel") < 0) EventInstance.Instance.InputEvent.OnPickItemEvent(true);

            //MOVEMENT
            if (Input.GetKey(KeyCode.Space)) EventInstance.Instance.InputEvent.OnJumpEvent();

            if (Input.GetKey(KeyCode.LeftShift)) EventInstance.Instance.InputEvent.OnSneakEvent();

            EventInstance.Instance.InputEvent.OnMoveVertical(Input.GetAxis("Vertical"));
            EventInstance.Instance.InputEvent.OnMoveHorizontal(Input.GetAxis("Horizontal"));

            //UI
            if (Input.GetKeyDown(KeyCode.F2)) EventInstance.Instance.InputEvent.OnToggleUIEvent();
            if (Input.GetKeyDown(KeyCode.Tab)) EventInstance.Instance.InputEvent.OnToggleMenuEvent();
        }
    }
}