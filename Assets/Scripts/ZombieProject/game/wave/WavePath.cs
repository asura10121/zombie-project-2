﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ZombieProject.game.checkpoint;
using Quaternion = UnityEngine.Quaternion;

namespace ZombieProject.game.wave
{
    public class WavePath : MonoBehaviour, IWavePath
    {
        [SerializeField] private Transform enemyCheckPointsTransform;
        [SerializeField] private TargetMob targetMobPrefab;

        private Dictionary<string, TargetMob> _checkPoints;

        private void Awake()
        {
            InitCheckPoints();
        }

        private void InitCheckPoints()
        {
            _checkPoints = new Dictionary<string, TargetMob>();

            foreach (var entranceType in (EntranceType[]) Enum.GetValues(typeof(EntranceType)))
            {
                var mobPaths = new List<MobCheckPoint>();
                foreach (Transform data in enemyCheckPointsTransform)
                {
                    var cp = data.GetComponent<MobCheckPoint>();
                    if (!cp || cp.entranceType != entranceType) continue;
                    mobPaths.Add(cp);
                }

                TargetMob cpLeft = null;
                TargetMob cpRight = null;
                TargetMob cpMid = null;
                for (var i = 0; i < mobPaths.Count; i++)
                {
                    var mobPath = mobPaths[i];
                    var mobPathTransform = mobPath.transform;
                    var spawnPos = mobPathTransform.position;
                    var padding = mobPathTransform.right * mobPath.mobPadding;
                    var cpLeftTemp = Instantiate(targetMobPrefab, spawnPos - padding, Quaternion.identity);
                    var cpRightTemp = Instantiate(targetMobPrefab, spawnPos + padding, Quaternion.identity);
                    var cpMidTemp = Instantiate(targetMobPrefab, spawnPos, Quaternion.identity);

                    //Link
                    if (cpLeft is { })
                    {
                        cpLeft.SetNextTarget(cpLeftTemp);
                        cpRight.SetNextTarget(cpRightTemp);
                        cpMid.SetNextTarget(cpMidTemp);
                    }

                    cpLeft = cpLeftTemp;
                    cpRight = cpRightTemp;
                    cpMid = cpMidTemp;

                    if (i != 0) continue;
                    _checkPoints.Add(GetPrimaryKey(mobPath.entranceType, PathType.LEFT), cpLeftTemp);
                    _checkPoints.Add(GetPrimaryKey(mobPath.entranceType, PathType.RIGHT), cpRightTemp);
                    _checkPoints.Add(GetPrimaryKey(mobPath.entranceType, PathType.MID), cpMidTemp);
                }
            }
        }

        public TargetMob GetStartPoint(EntranceType entranceType, PathType pathType)
        {
            var primaryKey = GetPrimaryKey(entranceType, pathType);
            return !_checkPoints.ContainsKey(primaryKey) ? null : _checkPoints[primaryKey];
        }

        private string GetPrimaryKey(EntranceType entranceType, PathType pathType)
        {
            return $"{entranceType}{pathType}";
        }
    }
}