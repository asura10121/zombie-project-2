﻿using UnityEngine;

namespace Singleton
{
    public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        public static T Instance;

        public virtual void Awake()
        {
            if (Instance != this && Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            name = $"{typeof(T).Name} (Mono Singleton)";
            Instance = GetComponent<T>();
        }
    }
}