﻿using System.Collections.Generic;

namespace Core.inventory.meta
{
    public interface IItemMeta
    {
        public string GetDisplayName();
        public void SetDisplayName(string displayName);
        public List<string> GetLore();
        public void SetLore(List<string> lore);
    }
}