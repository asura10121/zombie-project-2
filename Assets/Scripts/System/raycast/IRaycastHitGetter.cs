﻿using UnityEngine;

namespace System.raycast
{
    public interface IRaycastHitGetter
    {
        public RaycastHit GetRaycastHit();
    }
}