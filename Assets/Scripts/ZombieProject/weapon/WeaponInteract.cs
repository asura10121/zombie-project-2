﻿using Core.events;
using UnityEngine;

namespace ZombieProject.weapon
{
    public class WeaponInteract : MonoBehaviour, IWeaponInteract
    {
        private bool _triggerDown;
        private IWeapon _weapon;

        private void FixedUpdate()
        {
            if (!_triggerDown) return;
            if (!_weapon.GetWeaponSo().isAuto) _triggerDown = false;

            _weapon?.Shoot();
        }

        public void OnInteract(IWeapon weapon, ActionType actionType)
        {
            _weapon = weapon;

            switch (actionType)
            {
                case ActionType.LEFT_CLICK_DOWN:
                    _triggerDown = true;
                    break;
                case ActionType.LEFT_CLICK_UP:
                    _triggerDown = false;
                    break;
                case ActionType.RIGHT_CLICK_DOWN:
                    weapon.Reload();
                    break;
                case ActionType.RIGHT_CLICK_UP:
                    break;
            }
        }
    }
}