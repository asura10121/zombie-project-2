﻿using System.Collections.Generic;
using UnityEngine;
using ZombieProject.weapon.inventory.item;

namespace ZombieProject.weapon.factory
{
    public class WeaponCommandItemFactory : MonoBehaviour, IWeaponCommandItemFactory
    {
        public WeaponCommandItem Create(WeaponSo weaponSo)
        {
            var weapon = WeaponInstance.GetWeapon(weaponSo.weaponType);
            var weaponCommandItem = new WeaponCommandItem(weaponSo.enumValue, weapon);
            var itemMeta = weaponCommandItem.GetItemMeta();
            itemMeta.SetDisplayName(weaponSo.displayName);
            itemMeta.SetLore(new List<string>(weaponSo.description));
            return weaponCommandItem;
        }
    }
}