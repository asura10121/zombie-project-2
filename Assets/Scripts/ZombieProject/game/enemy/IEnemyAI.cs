﻿using ZombieProject.game.checkpoint;

namespace ZombieProject.game.enemy
{
    public interface IEnemyAI
    {
        void SetCheckPoint(TargetMob targetMob);
    }
}