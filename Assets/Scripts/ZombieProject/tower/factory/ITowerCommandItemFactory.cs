﻿using ZombieProject.tower.inventory.item;

namespace ZombieProject.tower.factory
{
    public interface ITowerCommandItemFactory : IFactory<TowerCommandItem, TowerSo>
    {
    }
}