﻿using UnityEngine;
using UnityEngine.UI;

namespace ZombieProject.ui.weaponbar
{
    public class WeaponReloadDisplay : MonoBehaviour, IWeaponReloadDisplay
    {
        [SerializeField] private Image reloadingImage;

        public void SetReloading(float time, float reloadTIme)
        {
            reloadingImage.fillAmount = (reloadTIme - time) / reloadTIme;
            if (time < 0.001f) reloadingImage.fillAmount = 0;
        }
    }
}