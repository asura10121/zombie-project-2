﻿using Core.events;
using Core.spectator;
using Core.ui;
using UnityEngine;
using ZombieProject.events;
using ZombieProject.weapon;
using ZombieProject.weapon.inventory.item;

namespace ZombieProject.ui.weaponbar
{
    public class WeaponUI : BaseUI
    {
        [SerializeField] private WeaponEventSo weaponEventSo;
        [SerializeField] private InventoryEventSO inventoryEventSo;

        private IWeapon _weapon;

        private IWeaponAmmoDisplay _weaponAmmoDisplay;
        private IWeaponReloadDisplay _weaponReloadDisplay;
        private IWeaponDisplay _weaponDisplay;

        public override void Awake()
        {
            base.Awake();
            _weaponAmmoDisplay = GetComponent<IWeaponAmmoDisplay>();
            _weaponReloadDisplay = GetComponent<IWeaponReloadDisplay>();
            _weaponDisplay = GetComponent<IWeaponDisplay>();
        }

        protected override void Start()
        {
            base.Start();
            Show();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            inventoryEventSo.PlayerItemHeldEvent += OnPlayerItemHeldEvent;
            weaponEventSo.AmmoChangeEvent += OnAmmoChangeEvent;
            weaponEventSo.ReloadingEvent += OnReloadingEvent;
            EventInstance.Instance.PlayerEvent.PlayerSpectateEvent += OnPlayerSpectateEvent;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            inventoryEventSo.PlayerItemHeldEvent -= OnPlayerItemHeldEvent;
            weaponEventSo.AmmoChangeEvent -= OnAmmoChangeEvent;
            weaponEventSo.ReloadingEvent -= OnReloadingEvent;
            EventInstance.Instance.PlayerEvent.PlayerSpectateEvent -= OnPlayerSpectateEvent;
        }

        private void OnPlayerItemHeldEvent(Core.entity.Player player, int previous, int current)
        {
            var item = player.GetInventory().GetItemInHand();
            if (!(item is WeaponCommandItem weaponCommandItem))
            {
                Hide();
                return;
            }

            Show();
            _weapon = weaponCommandItem.GetWeapon();
            _weaponAmmoDisplay.SetAmmo(_weapon);
            _weaponDisplay.SetWeapon(_weapon.GetWeaponSo());
        }

        private void OnAmmoChangeEvent(IWeapon weapon, int amount)
        {
            if (_weapon != weapon) return;
            _weaponAmmoDisplay.SetAmmo(weapon);
        }

        private void OnReloadingEvent(IWeapon weapon, float reload, float reloadTime)
        {
            if (_weapon != weapon) return;
            _weaponReloadDisplay.SetReloading(reload, reloadTime);
        }

        private void OnPlayerSpectateEvent(ISpectator spectator)
        {
            if (spectator is Core.entity.Player) Show();
            else Hide();
        }
    }
}