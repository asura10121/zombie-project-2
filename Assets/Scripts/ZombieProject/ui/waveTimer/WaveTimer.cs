﻿using UnityEngine;
using UnityEngine.UI;

namespace ZombieProject.ui.waveTimer
{
    public class WaveTimer : MonoBehaviour, IWaveTimer
    {
        [SerializeField] private Image waveTimerImage;

        private void Awake()
        {
            waveTimerImage.fillAmount = 0f;
        }

        public void SetWaveTimer(float current, float waveDelay)
        {
            waveTimerImage.fillAmount = current / waveDelay;
        }
    }
}